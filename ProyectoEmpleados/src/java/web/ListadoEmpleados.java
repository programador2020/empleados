package web;

import entidades.DB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ListadoEmpleados", urlPatterns = {"/ListadoEmpleados"})
public class ListadoEmpleados extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("Sistema de empleados");

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("Listado de empleados");

        String consultaSql = "SELECT * FROM `personal`;";

        try {
            Connection miConexion = DB.getInstance().getConnection();
            PreparedStatement miPreparativo = miConexion.prepareStatement(consultaSql);
            ResultSet miResultado = miPreparativo.executeQuery();

            out.println("<!DOCTYPE html>\n"
                    + "<html>\n"
                    + "    <head>\n"
                    + "        <title>Empleados - RRHH</title>\n"
                    + "        <meta charset=\"UTF-8\">\n"
                    + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                    + "        <link href=\"css/styles.css\" rel=\"stylesheet\" />\n"
                    + "    </head>\n"
                    + "    <body>");
            out.println("<div class=\"product_grid\">\n"
                    + "  <ul class=\"product_list list\">");
            while (miResultado.next()) {
                System.out.println(miResultado.getString("nombre"));
                out.println("<li class=\"product_item\">\n"
                        + "      <div class=\"product_sale\">\n"
                        + "        <p>On Sale</p>\n"
                        + "      </div>\n"
                        + "      <div class=\"product_image\">\n"
                        + "        <a href=\"#\"><img src=\"https://bit.ly/1myplK1\" alt=\"\"></a>\n"
                        + "          <div class=\"product_buttons\">\n"
                        + "            <button class=\"product_heart\"><i class=\"fa fa-heart\"></i></button>\n"
                        + "            <button class=\"product_compare\"><i class=\"fa fa-random\"></i></button>\n"
                        + "            <button class=\"add_to_cart\"><i class=\"fa fa-shopping-cart\"></i></button>\n"
                        + "            <div class=\"quick_view\">\n"
                        + "              <a href=\"#\"><h6>Quick View</h6></a>\n"
                        + "            </div>\n"
                        + "          </div>\n"
                        + "      </div>\n"
                        + "      <div class=\"product_values\">\n"
                        + "        <div class=\"product_title\">\n"
                        + "          <h5>" + miResultado.getString("nombre") + "</h5>\n"
                        + "        </div>\n"
                        + "        <div class=\"product_price\">\n"
                        + "          <a href=\"#\"> <span class=\"price_new\">" + miResultado.getString("dni") + "</span></a>\n"
                        + "           <span class=\"product_rating\"></span>\n"
                        + "        </div>\n"
                        + "        <div class=\"product_desc\">\n"
                        + "          <p class=\"truncate\">Nacimiento: " + miResultado.getString("fecha_nacimiento") + "</p>\n"
                                + "          <p class=\"truncate\">Legajo: " + miResultado.getString("legajo") + "</p>\n"
                        + "        </div>\n"
                        + "        <div class=\"product_buttons\">\n"
                        + "          <button class=\"product_heart\"><i class=\"fa fa-heart\"></i></button>\n"
                        + "          <button class=\"product_compare\"><i class=\"fa fa-random\"></i></button>\n"
                        + "          <button class=\"add_to_cart\"><i class=\"fa fa-shopping-cart\"></i></button>\n"
                        + "        </div>\n"
                        + "      </div>\n"
                        + "    </li>");
            }
            out.println("  </ul>\n"
                    + "</div>");
            out.println("</body>\n"
                    + "</html>");

        } catch (SQLException miError) {
            System.out.println(miError);
        } catch (ClassNotFoundException miError) {
            System.out.println(miError);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
