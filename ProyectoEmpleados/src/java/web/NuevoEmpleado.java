package web;

import entidades.DB;
import entidades.Empleados;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "NuevoEmpleado", urlPatterns = {"/NuevoEmpleado"})
public class NuevoEmpleado extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("Sistema de empleados");
        Empleados miEmpleado = new Empleados();
        miEmpleado.nombre = request.getParameter("nombre");
        miEmpleado.legajo = request.getParameter("legajo");
        miEmpleado.fechaNacimiento = request.getParameter("nacimiento");
        miEmpleado.dni = request.getParameter("dni");
        System.out.println(miEmpleado);
        String consultaSql = "INSERT INTO `personal` (`nombre`, `legajo`, `fecha_nacimiento`, `dni`, `id`) VALUES ('" + miEmpleado.nombre + "', '" + miEmpleado.legajo + "', '" + miEmpleado.fechaNacimiento + "', '" + miEmpleado.dni + "', NULL);";

        try {
            Connection miConexion = DB.getInstance().getConnection();
            PreparedStatement miPreparacion = miConexion.prepareStatement(consultaSql);
            miPreparacion.execute();
            response.sendRedirect(request.getContextPath() + "/ListadoEmpleados");
            System.out.println("Agregaste bien al empleado");
        } catch (SQLException miAlerta) {
            System.out.println(miAlerta);
        } catch (ClassNotFoundException miAlerta) {
            System.out.println(miAlerta);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
